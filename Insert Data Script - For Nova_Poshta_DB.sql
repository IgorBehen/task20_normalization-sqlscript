-- city
-- INSERT INTO city (city_name) VALUES ('Lviv');
-- INSERT INTO city (city_name) VALUES ('Kyiv');
-- INSERT INTO city (city_name) VALUES ('Dnipro');
-- INSERT INTO city (city_name) VALUES ('Odesa');
-- INSERT INTO city (city_name) VALUES ('Vinnycia');
-- INSERT INTO city (city_name) VALUES ('Mykolajiv');
-- INSERT INTO city (city_name) VALUES ('Lyuc`k');
-- INSERT INTO city (city_name) VALUES ('Zaporizhzhia');
-- INSERT INTO city (city_name) VALUES ('Ternopil');
-- INSERT INTO city (city_name) VALUES ('Ivano-Frankivs`k');
-- discount
-- INSERT INTO discount (name, `cost(%)`) VALUES ('2%', 2);
-- INSERT INTO discount (name, `cost(%)`) VALUES ('5%', 5);
-- INSERT INTO discount (name, `cost(%)`) VALUES ('10%', 10);
-- INSERT INTO discount (name, `cost(%)`) VALUES ('20%', 20);
-- INSERT INTO discount (name, `cost(%)`) VALUES ('30%', 30);
-- INSERT INTO discount (name, `cost(%)`) VALUES ('35%', 35);
-- INSERT INTO discount (name, `cost(%)`) VALUES ('45%', 45);
-- INSERT INTO discount (name, `cost(%)`) VALUES ('50%', 50);
-- INSERT INTO discount (name, `cost(%)`) VALUES ('65%', 65);
-- INSERT INTO discount (name, `cost(%)`) VALUES ('70%', 70);
-- client
-- INSERT INTO client (name, surname, phone, discount_id, street, number, city_id) VALUES ('Ivan', 'Mel`nyk', '0674675983', 2, 'Shevchenka', '12', 5);
-- INSERT INTO client (name, surname, phone, discount_id, street, number, city_id) VALUES ('Petro', 'Kovalenko', '0504675567', 5, 'Franka', '34', 8);
-- INSERT INTO client (name, surname, phone, discount_id, street, number, city_id) VALUES ('Nazar', 'Bondarenko', '0634675234', 3, 'Nova', '1', 3);
-- INSERT INTO client (name, surname, phone, discount_id, street, number, city_id) VALUES ('Andriy', 'Tkachenko', '0674675876', 9, 'Velyka', '19', 9);
-- INSERT INTO client (name, surname, phone, discount_id, street, number, city_id) VALUES ('Taras', 'Oliynyk', '0634675268',  2, 'Vesela', '35', 6);
-- INSERT INTO client (name, surname, phone, discount_id, street, number, city_id) VALUES ('Olia', 'Shevchuk', '0964675957', 4, 'Shyroka', '7', 10);
-- INSERT INTO client (name, surname, phone, discount_id, street, number, city_id) VALUES ('Mariya', 'Koval`', '0674675608', 10, 'Vyshneva', '16', 1);
-- INSERT INTO client (name, surname, phone, discount_id, street, number, city_id) VALUES ('Galia', 'Moroz', '0504675876', 7, 'Malynova', '21', 2);
-- INSERT INTO client (name, surname, phone, discount_id, street, number, city_id) VALUES ('Nadiya', 'Marchenko', '0974675149', 3, 'Lypova', '2', 6);
-- INSERT INTO client (name, surname, phone, discount_id, street, number, city_id) VALUES ('Yulia', 'Lysenko', '0664675586', 1, 'Dovga', '45',2 );
-- vehicle
-- INSERT INTO vehicle (name) VALUES ('bike');
-- INSERT INTO vehicle (name) VALUES ('сar');
-- INSERT INTO vehicle (name) VALUES ('truck');
-- courier
-- INSERT INTO courier (name, ` surname`, phone, vehicle_id) VALUES ('Roman', 'Shvec', '0504573834', 1);
-- INSERT INTO courier (name, ` surname`, phone, vehicle_id) VALUES ('Yurij', 'Kuz`menko', '0505679843', 3);
-- INSERT INTO courier (name,  ` surname`, phone, vehicle_id) VALUES ('Dmytro', 'Savchuk', '0675436895', 2);
-- INSERT INTO courier (name,  ` surname`, phone, vehicle_id) VALUES ('Danylo', 'Kushnir', '0662459784', 1);
-- INSERT INTO courier (name,  ` surname`, phone, vehicle_id) VALUES ('Bohdan', 'Sydorenko', '0637659843', 2);
-- INSERT INTO courier (name,  ` surname`, phone, vehicle_id) VALUES ('Sofija', 'Romanchuk', '0950197568', 3);
-- INSERT INTO courier (name,  ` surname`, phone, vehicle_id) VALUES ('Anna', 'Kulyk', '0664579837', 1);
-- INSERT INTO courier (name,  ` surname`, phone, vehicle_id) VALUES ('Kateryna', 'Kravec`', '0677894657', 1);
-- INSERT INTO courier (name,  ` surname`, phone, vehicle_id) VALUES ('Vira', 'Bilous', '0635769285', 2);
-- INSERT INTO courier (name,  ` surname`, phone, vehicle_id) VALUES ('Daryna', 'Kostiuk', '0509783456', 1);
-- offices 
-- INSERT INTO offices (name,  city, street, number) VALUES ('№1', 'Lviv', 'Shevchenka', '1');
-- INSERT INTO offices (name,  city, street, number) VALUES ('№2', 'Lviv', 'Nezalezhnosti', '10');
-- INSERT INTO offices (name,  city, street, number) VALUES ('№3', 'Lviv', 'Gorodocka', '31');
-- INSERT INTO offices (name,  city, street, number) VALUES ('№4', 'Kyiv', 'Hreschatyk', '22');
-- INSERT INTO offices (name,  city, street, number) VALUES ('№5', 'Kyiv', 'Vasylkivska', '35');
-- INSERT INTO offices (name,  city, street, number) VALUES ('№6', 'Kyiv', 'Nezalezhnosti', '3');
-- INSERT INTO offices (name,  city, street, number) VALUES ('№7', 'Odesa', 'Franka', '56');
-- INSERT INTO offices (name,  city, street, number) VALUES ('№8', 'Odesa', 'Shevchenka', '64');
-- INSERT INTO offices (name,  city, street, number) VALUES ('№9', 'Odesa', 'Lypova', '76');
-- INSERT INTO offices (name,  city, street, number) VALUES ('№10', 'Dnipro', 'Shyroka', '83');
-- internal_invoice
-- INSERT INTO internal_invoice (num, departure_offices_id, destination_office_id) VALUES ('031', 1, 5);
-- INSERT INTO internal_invoice (num, departure_offices_id, destination_office_id) VALUES ('032', 11, 12);
-- INSERT INTO internal_invoice (num, departure_offices_id, destination_office_id) VALUES ('033', 9, 7);
-- INSERT INTO internal_invoice (num, departure_offices_id, destination_office_id) VALUES ('034', 5, 13);
-- INSERT INTO internal_invoice (num, departure_offices_id, destination_office_id) VALUES ('035', 1, 9);
-- INSERT INTO internal_invoice (num, departure_offices_id, destination_office_id) VALUES ('036', 5, 11);
-- INSERT INTO internal_invoice (num, departure_offices_id, destination_office_id) VALUES ('037', 1, 6);
-- INSERT INTO internal_invoice (num, departure_offices_id, destination_office_id) VALUES ('038', 8, 10);
-- INSERT INTO internal_invoice (num, departure_offices_id, destination_office_id) VALUES ('039', 6, 1);
-- INSERT INTO internal_invoice (num, departure_offices_id, destination_office_id) VALUES ('040', 1, 13);
-- operator  
 -- INSERT INTO operator (name, surname, phone, offices_id) VALUES ('Marija', 'Tarasenko', '0508743657', 1);
 -- INSERT INTO operator (name, surname, phone, offices_id) VALUES ('Marta', 'Makarenko', '0634679786', 11);
 -- INSERT INTO operator (name, surname, phone, offices_id) VALUES ('Hrystyna', 'Tkachuk', '0952586973', 5);
 -- INSERT INTO operator (name, surname, phone, offices_id) VALUES ('Anna', 'Shl`ha', '0974538674', 6);
 -- INSERT INTO operator (name, surname, phone, offices_id) VALUES ('Viktoriya', 'Radchenko', '0660978562', 9);
 -- INSERT INTO operator (name, surname, phone, offices_id) VALUES ('Dmytro', 'Holub', '0675769306', 13);
 -- INSERT INTO operator (name, surname, phone, offices_id) VALUES ('Danylo', 'Babych', '0508745728', 7);
 -- INSERT INTO operator (name, surname, phone, offices_id) VALUES ('Yurij', 'Maksymenko', '0957452897', 12);
 -- INSERT INTO operator (name, surname, phone, offices_id) VALUES ('Bohdan', 'Velychko', '0968321564', 8);
 -- INSERT INTO operator (name, surname, phone, offices_id) VALUES ('Roman', 'Shpak', '0974562786', 10);
-- parcel_category  
-- INSERT INTO parcel_category (num, min_size, max_size, min_weight, max_weight) VALUES ('s', 0.1, 1.0, 0.100, 1.0);
-- INSERT INTO parcel_category (num, min_size, max_size, min_weight, max_weight) VALUES ('m', 1.0, 50.0, 1.0, 50.0);
-- INSERT INTO parcel_category (num, min_size, max_size, min_weight, max_weight) VALUES ('l', 50.0, 100.0, 50.0, 100.0);
-- INSERT INTO parcel_category (num, min_size, max_size, min_weight, max_weight) VALUES ('xl', 100.0, 500.0, 100.0, 500.0);
-- INSERT INTO parcel_category (num, min_size, max_size, min_weight, max_weight) VALUES ('xxl', 500.0, 200.0, 500.0, 200.0);
-- parcel  
-- INSERT INTO parcel (num, info, parcel_category_id) VALUES ('034675', '-', 3);
-- INSERT INTO parcel (num, info, parcel_category_id) VALUES ('034676', '-', 2);
-- INSERT INTO parcel (num, info, parcel_category_id) VALUES ('034677', '-', 5);
-- INSERT INTO parcel (num, info, parcel_category_id) VALUES ('034678', '-', 1);
-- INSERT INTO parcel (num, info, parcel_category_id) VALUES ('034679', '-', 4);
-- INSERT INTO parcel (num, info, parcel_category_id) VALUES ('034680', '-', 2);
-- INSERT INTO parcel (num, info, parcel_category_id) VALUES ('034681', '-', 5);
-- INSERT INTO parcel (num, info, parcel_category_id) VALUES ('034681', '-', 3);
-- INSERT INTO parcel (num, info, parcel_category_id) VALUES ('034683', '-', 1);
-- INSERT INTO parcel (num, info, parcel_category_id) VALUES ('034684', '-', 2);
-- order
-- INSERT INTO `order` (num, price, sum, date, courier_id, parcel_id, client_id, `internal_invoice_ id`) VALUES ('0007645', 75.45, 75.45, 20191017, 11, 1, 6, 1);
-- INSERT INTO `order` (num, price, sum, date, courier_id, parcel_id, client_id, `internal_invoice_ id`) VALUES ('0007645', 95.00, 95.00, 20191017, 20, 6, 3, 1);
-- INSERT INTO `order` (num, price, sum, date, courier_id, parcel_id, client_id, `internal_invoice_ id`) VALUES ('0007645', 60.50, 60.50, 20191017, 13, 3, 2, 1);
-- INSERT INTO `order` (num, price, sum, date, courier_id, parcel_id, client_id, `internal_invoice_ id`) VALUES ('0007645', 85.65, 85.65, 20191017, 19, 9, 10, 1);
-- INSERT INTO `order` (num, price, sum, date, courier_id, parcel_id, client_id, `internal_invoice_ id`) VALUES ('0007645', 55.55, 55.50, 20191017, 12, 5, 8, 1);
-- INSERT INTO `order` (num, price, sum, date, courier_id, parcel_id, client_id, `internal_invoice_ id`) VALUES ('0007645', 50.00, 50.50, 20191017, 17, 10, 7, 1);
-- INSERT INTO `order` (num, price, sum, date, courier_id, parcel_id, client_id, `internal_invoice_ id`) VALUES ('0007645', 90.50, 90.50, 20191017, 15, 3, 4, 1);
-- INSERT INTO `order` (num, price, sum, date, courier_id, parcel_id, client_id, `internal_invoice_ id`) VALUES ('0007645', 70.35, 79.35, 20191017, 12, 7, 9, 1);
-- INSERT INTO `order` (num, price, sum, date, courier_id, parcel_id, client_id, `internal_invoice_ id`) VALUES ('0007645', 75.00, 75.00, 20191017, 14, 2, 5, 1);
-- INSERT INTO `order` (num, price, sum, date, courier_id, parcel_id, client_id, `internal_invoice_ id`) VALUES ('0007645', 55.55, 55.55, 20191017, 16, 8, 1, 1);
-- ????operator_has_order???
-- INSERT INTO operator_has_order (operator_id, operator_offices_id, order_id) VALUES (9, 1, 1);
-- INSERT INTO operator_has_order (operator_id, operator_offices_id, order_id) VALUES (3, 2, 2);
-- INSERT INTO operator_has_order (operator_id, operator_offices_id, order_id) VALUES (1, 3, 3);
-- INSERT INTO operator_has_order (operator_id, operator_offices_id, order_id) VALUES (5, 4, 4);
-- INSERT INTO operator_has_order (operator_id, operator_offices_id, order_id) VALUES (6, 5, 5);
-- INSERT INTO operator_has_order (operator_id, operator_offices_id, order_id) VALUES (2, 6, 6);
-- INSERT INTO operator_has_order (operator_id, operator_offices_id, order_id) VALUES (7, 7, 7);
-- INSERT INTO operator_has_order (operator_id, operator_offices_id, order_id) VALUES (10, 8, 8);
-- INSERT INTO operator_has_order (operator_id, operator_offices_id, order_id) VALUES (8, 9, 9);
-- INSERT INTO operator_has_order (operator_id, operator_offices_id, order_id) VALUES (4, 10, 10);
-- ???



select * from operator_has_order;
select * from operator_has_order;
select * from operator_has_order;
select * from operator_has_order;
select * from operator_has_order;
select * from operator_has_order;
select * from operator_has_order;
select * from operator_has_order;
select * from operator_has_order;
select * from operator_has_order;



